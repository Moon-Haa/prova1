package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Primeira avaliação parcial 2014/2.
 * @author 
 */
public class Complexo {
    private double real;
    private double img;

    public Complexo() {
    }

    public Complexo(double real, double img) {
        this.real = real;
        this.img = img;
    }

    // implementar getReal()
    public double getReal() {
        return this.real;
    }

    // implementar getImg()
    public double getImg() {
        return this.img;
    }

    public Complexo soma(Complexo c) {
        return new Complexo(real + c.real, img + c.img);
    }
    
    // implementar sub(Complexo)
    public Complexo sub(Complexo c) {
        return new Complexo(this.real - c.real, this.img - c.img);
    }

    // implementar prod(double)
    public Complexo prod(double r) {
        return new Complexo(this.real*r,this.img*r);
    }

    // implementar prod(Complexo)
    public Complexo prod(Complexo c) {
        return new Complexo(this.real*c.real - this.img*c.img,this.img*c.real + this.real*c.img);
    }
    
    // implementar div(Complexo)
    public Complexo div(Complexo c) {
        double realPart = (this.real*c.real + this.img*c.img)/(c.real*c.real + c.img*c.img);
        double imgPart = (c.real*this.img - this.real*c.img)/(c.real*c.real + c.img*c.img);
        return new Complexo(realPart,imgPart);
    }
    
    // implementar sqrt()
    public Complexo[] sqrt() {
        double arcOrig = 0;
        double p = Math.sqrt(Math.sqrt(this.real*this.real + this.img*this.img));
        if(this.real > 0 )
            arcOrig = Math.atan(this.img/this.real);
        else if(this.real < 0)
            arcOrig = Math.atan(this.img/this.real) + Math.PI;
        else
        {
            if(this.img > 0)
                arcOrig = Math.PI/2;
            else if(this.img < 0)
                arcOrig = -Math.PI/2;
            else
                arcOrig = 0;
        }
        // completar implementação
        // retornar o vetor contendo as raízes
        Complexo a = new Complexo(p*Math.cos(arcOrig/2),p*Math.sin(arcOrig/2));
        Complexo b = new Complexo(p*Math.cos(arcOrig/2+Math.PI),p*Math.sin(arcOrig/2+Math.PI));
        Complexo vet[] = new Complexo[2];
        vet[0] = a;
        vet[1] = b;
        return vet;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (Double.doubleToLongBits(real)
            ^ (Double.doubleToLongBits(real) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(img)
            ^ (Double.doubleToLongBits(img) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Complexo c = (Complexo) obj;
        return obj != null && getClass() == obj.getClass()
            && real == c.real && img == c.img;
    }

    @Override
    public String toString() {
        return String.format("%+f%+fi", real, img);
    }
}
