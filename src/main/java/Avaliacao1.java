
import utfpr.ct.dainf.if62c.avaliacao.Complexo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Primeira avaliação parcial 2014/2.
 * @author 
 */
public class Avaliacao1 {

    public static void main(String[] args) {
        // implementar main
        Complexo eq1[] = raizesEquacao(new Complexo(1,0), new Complexo(5,0), new Complexo(4,0));
        Complexo eq2[] = raizesEquacao(new Complexo(1,0), new Complexo(2,0), new Complexo(5,0));
        System.out.println("x1="+eq1[0].toString());
        System.out.println("x2="+eq1[1].toString());
        System.out.println("y1="+eq2[0].toString());
        System.out.println("y2="+eq2[1].toString());
    }
    
    // implementar raizesEquacao(Complexo, Complexo, Complexo)
    public static Complexo[] raizesEquacao(Complexo a,Complexo b,Complexo c) {
        Complexo delta = b.prod(b).sub(a.prod(c).prod(4));
        Complexo vet[] = delta.sqrt();
        vet[0] = vet[0].soma(b.prod(-1));
        vet[1] = vet[1].soma(b.prod(-1));
        vet[0] = vet[0].div(a.prod(2));
        vet[1] = vet[1].div(a.prod(2));
        return vet;
    }
}
